# ⚙️ Building ScreenPlay
## 1. 🚀 Setup build environment
We use a modern set of tools like CMake and Qt6 to create ScreenPlay. The setup is mostly automated. To get an overview of ScreenPlays architecture, see [Project Architecture](/ScreenPlayDocs/Project Architecture/)

1. Download and install the tools for your platform __first__:
    1. Setp for 🪟 [Windows](#windows)
    1. Setp for 🐧 [Linux](#linux)
    1. Setp for 🍏 [MacOS](#macos)
2. How to ⬇️ [clone ScreenPlay and dependencies](#clone-screenplay)
3. How to ⚙️ [compile ScreenPlay](#compile)


### Git setup
Enable git credential save, so you do not have to enter your password every time.
``` bash
git config --global credential.helper cache
```

### 🪟 Windows
1. Download and install:
   1. Microsoft Visual Studio Community 2022 [Download](https://visualstudio.microsoft.com/de/vs/community/)
      1. Make sure you have the component `Desktop development with C++` installed
   2. Python 3.11+ and select `Add to Path` during the installation. [Download](https://www.python.org/downloads/)
   3. Optional if you do not have tools like `git` or `cmake` installed, we recommend `Chocolatey`:
      1. Chocolatey via Powershell (Administrator) [Download](https://chocolatey.org/install)
      2. Run `choco.exe install git vscode cmake --installargs 'ADD_CMAKE_TO_PATH=System' -y`

### 🐧 Linux
Install dependencies for Debian/Ubuntu:
``` bash
sudo apt install cmake libwayland-dev wayland-protocols curl wget zip unzip tar git pkg-config libxcb-* libfontconfig-dev apt-transport-https ca-certificates gnupg  software-properties-common python3 python3-pip build-essential libgl1-mesa-dev mesa-common-dev lld ninja-build libxkbcommon-* libx11-dev xserver-xorg-dev xorg-dev -y
```
Install external dependencies for Wayland support before compiling
``` bash
cd ThirdParty
git clone https://invent.kde.org/frameworks/extra-cmake-modules.git
cd extra-cmake-modules
cmake configure .
make
sudo make install
# back to ScreenPlay/ThirdParty
cd ..
git clone https://invent.kde.org/plasma/layer-shell-qt.git
cd layer-shell-qt
cmake configure . -DCMAKE_PREFIX_PATH="./../../aqt/6.6.0/gcc_64"
make
sudo make install
``` 

### 🍏 MacOS
1. Install XCode 14+ , open and restart your device.
1. Install [brew](https://brew.sh) that is needed by some third party vcpkg packages. Do not forget to add brew to your path as outlined at the on of the installation!
    - `brew install pkg-config git llvm cmake`

### ⬇️ Clone ScreenPlay
``` bash
git clone https://gitlab.com/kelteseth/ScreenPlay.git ScreenPlay/ScreenPlay
```

### ⬇️ Downloading dependencies
Downloading dependencies is 100% automated. Simply run the `setup.py` script from the `Tools` folder:
### 🐍 Setup python virtual env
``` bash
# For Ubuntu 22.04 and newer we need to create an virutal env
sudo apt install python-is-python3 python3.11-venv -y
# Run this command in the ScreenPlay source root folder:
python -m venv .venv
# 🐧 Linux and 🍏 macOS
source .venv/bin/activate
# 🪟 Windows
.\venv\Scripts\Activate.ps1  
```
### 📦 Install global dependencies
Note: The python virtual env must be enabled, see above!
``` bash
cd Tools
python -m pip install -r requirements.txt
python setup.py
```



## 2. ⚙️ Compile
Now you can either use VSCode or QtCreator:
### VSCode
1. Open VSCode
1. Install the recommended extentions:
    1. [C/C++ for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
    1. [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
2. Press: `CTRL` + `SHIFT` + `P` for every command:
    1. `CMake: Select Configure Preset`. Select one of the listed presets like `MSVC SP Qt 6.6.0 Debug`
3. Press `F7` to Compile and `F5` to run!

### QtCreator
1. Open QtCreator
2. Select the imported temporary kit like `MSVC SP Qt 6.6.0 Debug`
3. Press `CTRL` + `R` to compile and run!

### Developer docs 
We use qdoc to generate documentation.

`qdoc.exe configWindows.qdocconf`

If you have installed Qt in a different directory, change the path to your Qt location.

Some useful links:

- [Introduction to QDoc](https://doc.qt.io/qt-6/01-qdoc-manual.html)
- [Writing qdoc comments](https://doc.qt.io/qt-6/qdoc-guide-writing.html)

