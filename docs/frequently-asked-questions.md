# ❓ Frequently Asked Questions
### ScreenPlay shows no installed wallpapers even thought I installed some via the Steam Workshop!

This bug can happen sometimes and we are working on a solution! It happens when we cannot resolve the Steam Workshop path. A fix for this is quite simple. Just set the path in the settings to:

 * **C:\Program Files (x86)\Steam\steamapps\workshop\content\672870**
 
 for MacOS ⌘ CMD+⇧ SHIFT+. reveals hidden files in Finder and Open/Save dialogs.
 
  * **/Users/_USERNAME_/Library/Application Support/Steam/steamapps/workshop/content/672870**

Of course if you have Steam installed somewhere else the basepath is different!

![Correct Steam Workshop path](steam_workshop_path.png "steam_workshop_path")

### Does ScreenPlay work on older Windows Versions?

*  ScreenPlay does not start on Win xp (thank god)
*  ScreenPlay does start on Windows 7 but the Wallpaper are displayed wrong
*  ScreenPlay was never tested on Windows 8.1 because of the low user count

### Does ScreenPlay work on Linux with Proton/Wine?

Proton is Valves way to make none Linux apps working on Linux. This cannot be disabled by the developer. ScreenPlay does not provide binaries for linux **yet**. Running on proton/wine is and never will be supported. 

### Does ScreenPlay support my Linux distro/desktop environment

ScreenPlay should be able to support all major modern Linux distributions. Most of the time we have to manually code for every desktop environment separate integration. Every DE handles the background rendering differently.

### Why does ScreenPlay consume so much CPU usage?

It is because your graphics cards cannot hardware decode the video. This means your CPU has to do the job. You can ether try to import the video again with different settings like VP8 or VP9. You can also try another type of wallpaper like QML or HTML. 
Please see the [supportet hardware decoding charts.](/ScreenPlayDocs/wallpaper/wallpaper)

Since version 0.10.0 (23.02.2020) there is an setting for disable the video rendering (audo will continue to play!) when not seeing the wallpaper. This causes the CPU and GPU usage for the wallpaper to drop to 0%!

Since version 0.13.0 (01.02.2021) we support gif Wallpaper. For rendering gif we do not use a WebEngine (Chromium) process. Rendering gifs is the fastest rendering type of Wallpaper, with about 0.1% CPU usage!

### How do I delete wallpaper or widgets?

Simply delete the folder where the wallpaper/widget is located. You might need to close ScreenPlay for this because it uses some files like the preview thumbnail. To locate the wallpaper path simply right click on a wallpaper and click on "Open containing folder":
![delte_content](delte_content.png "delte_content")

### Why does ScreenPlay not close itself when I press close?

Because it needs to run in the background for you wallpaper and widgets to work. Sadly there is no dedicated button for minimize to system tray on windows, so this is a workaround. If you want to close ScreenPlay for good you can simply right click on the Windows tray icon and select "Quit"

![close_sp](close_sp.png "close_sp")

### Can I import wallpaper from Wallpaper Engine?
It depends on the wallpaper. If it is a video wallpaper then you could use http://steamworkshop.download/ and import it via the create tab. 
If it is a scene wallpaper (*.pkg) then it won't work because of many criterias like we do not provide the propritary shaders from wallpaper engine.


### How can I enable Steam nightly builds?
ScreenPlay -> Settings -> Betas -> Internal

![nightly.png](./nightly.png)

### Something is wrong where does ScreenPlay save my settings?

##### Windows registry

*  Paths we need for the Steam workshop are located at:
    *  **Computer\HKEY_CURRENT_USER\Software\ScreenPlay\ScreenPlay** 
*  My autostart isn't working. On windows we add a registry entry to. With ScreenPlay as **key** and the **path** to the exe as value:
    *  **HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run** 
    *  Most of the times simple toggle the setting on -> off -> on to fix this

*  profiles.json
    *  Porfiles for widgets and wallpaper 

``` json
// default empty profiles
{
  "version":"1.0.0",
  "profilesWallpaper":[

 ],
 "profilesWidgets":[
    {

    }
 ],
 "profilesAppDrawer":[
    {

    }
 ]
}

```
