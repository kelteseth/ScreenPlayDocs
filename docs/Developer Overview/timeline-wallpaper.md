# ⏱️ Timeline

``` mermaid
sequenceDiagram
    participant U as UI
    participant TM as TimelineManager
    participant WP as WallpaperSection
    participant SDK as SDKConnection
    
    Note over U,SDK: Add New Wallpaper Workflow
    U->>+TM: setWallpaperAtTimelineIndex(type, path, ...)
    TM->>WP: startWallpaper(wallpaperData)
    WP->>SDK: start process
    SDK-->>WP: process connected
    WP-->>TM: wallpaper started
    TM-->>-U: success

    Note over U,SDK: Remove Wallpaper Workflow
    U->>+TM: removeWallpaperAt(timelineIndex, sectionId, monitorIndex)
    TM->>WP: removeWallpaper(monitorIndex)
    WP->>SDK: close connection
    SDK-->>WP: disconnected
    WP-->>TM: wallpaper removed
    TM-->>-U: success

    Note over U,SDK: Modify Timeline Workflow
    U->>+TM: moveTimelineAt(index, id, position, time)
    TM->>TM: validateTimelineSections()
    TM->>WP: checkActiveWallpaperTimeline()
    alt Timeline Change Needed
        WP->>SDK: deactivate current
        WP->>SDK: activate new
    end
    TM-->>-U: success
```
# Wallpaper Manager Internal Flows

## 1. Adding a New Wallpaper

### Entry Point
```cpp
setWallpaperAtTimelineIndex(
    type,           // ContentTypes::InstalledType
    absolutePath,   // Wallpaper file path
    previewImage,   // Preview image path
    file,          // Main file name
    title,         // Display title
    monitorIndex,  // Vector of monitor indices
    timelineIndex, // Timeline section index
    identifier,    // Section identifier
    saveToProfiles // Whether to persist changes
)
```

### Internal Flow
1. **Validation Phase**
   - Check path validity
   - Validate timeline index and identifier
   - Verify monitor indices

2. **Timeline Section Management**
   - Locate target timeline section
   - Validate section exists and can accept wallpaper
   - Check for existing wallpapers on target monitors

3. **Wallpaper State Handling**
   - If timeline section is active:
     ```cpp
     for (auto& activeWallpaper : activeTimeline->activeWallpaperList) {
         if (activeWallpaper->monitors() == wallpaperData.monitors()) {
             if (isSameWallpaperRuntime(activeWallpaper->type(), wallpaperData.type())) {
                 // Replace content in existing wallpaper
                 activeWallpaper->replace(wallpaperData);
             } else {
                 // Remove and restart with new type
                 await removeWallpaper(wallpaperData.monitors().first());
                 await startWallpaper(wallpaperData);
             }
         }
     }
     ```
   - If timeline section is inactive:
     - Store wallpaper data for later activation

4. **UI Updates**
   - Update monitor list model
   - Refresh timeline visualization
   - Trigger UI updates for affected monitors

5. **Persistence**
   - If saveToProfiles is true:
     - Update profiles configuration
     - Write changes to disk
     - Emit save confirmation

## 2. Removing a Wallpaper

### Entry Point
```cpp
removeWallpaperAt(
    timelineIndex,      // Timeline section index
    sectionIdentifier,  // Section identifier
    monitorIndex        // Target monitor
)
```

### Internal Flow
1. **Section Location**
   ```cpp
   auto section = wallpaperSection(timelineIndex, sectionIdentifier);
   if (!section) {
       return false;
   }
   ```

2. **Active Wallpaper Handling**
   - If section is active:
     - Stop wallpaper process
     - Wait for SDK disconnection
     - Clean up resources
     - Update active wallpaper list

3. **Data Cleanup**
   - Remove wallpaper data from section
   - Update monitor assignments
   - Clean up any orphaned references

4. **State Synchronization**
   - Update monitor model data
   - Refresh timeline section data
   - Trigger UI updates

## 3. Timeline Modification

### Entry Point
```cpp
moveTimelineAt(
    index,             // Timeline index
    identifier,        // Section identifier
    relativePosition,  // New position (0-1)
    positionTimeString // Time string (HH:MM:SS)
)
```

### Internal Flow
1. **Time Validation**
   ```cpp
   const QTime newPositionTime = QTime::fromString(positionTimeString, "hh:mm:ss");
   if (!newPositionTime.isValid()) {
       return false;
   }
   ```

2. **Timeline Updates**
   - Update section end time
   - Update relative position
   - Adjust adjacent section times

3. **Section Validation**
   - Ensure continuous coverage
   - Verify no overlaps
   - Check time constraints
   ```cpp
   validateTimelineSections();
   ```

4. **Active Wallpaper Management**
   - Check if change affects active section
   - Handle wallpaper state transitions
   - Update active wallpaper timings

5. **State Persistence**
   - Update timeline model
   - Save timeline configuration
   - Emit timeline update events

## Important Considerations

### Error Handling
- All operations should handle failures gracefully
- Maintain system state consistency
- Provide meaningful error messages
- Roll back partial changes on failure

### Async Operations
- Use QCoro tasks for async operations
- Properly await completion of all async tasks
- Handle cancellation and timeouts
- Maintain state during async transitions

### State Management
- Track current timeline section state
- Monitor wallpaper process health
- Handle SDK connection lifecycle
- Manage resource cleanup

### Timeline Integrity
- Maintain continuous coverage (00:00:00 to 23:59:59)
- Prevent overlapping sections
- Validate all time boundaries
- Ensure proper section ordering