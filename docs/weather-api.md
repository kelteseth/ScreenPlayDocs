# 🌤️ Weather API

## Simple interactive button example

``` qml
import QtQuick
import QtQuick.Controls
import ScreenPlayWeather

Item {
    id:root
    anchors.fill:parent

    ScreenPlayWeather {
        id: weather
        city: "Friedrichshafen"
        onReady: rp.model = weather.days;
        
    }
}

```
