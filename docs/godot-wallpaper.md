# 🎮 Godot Wallpapers

!!! tip  "🚀 Premium Feature"
    Godot wallpaper support is a premium feature. Your purchase helps tremendously in funding continued development - as a solo developer, every contribution directly supports new features and improvements. If you'd prefer, you can also compile ScreenPlay from source to unlock this feature for free - I believe in keeping this software open and accessible to everyone.

    ~ [Kelteseth](https://bsky.app/profile/kelteseth.bsky.social)

ScreenPlay supports creating dynamic wallpapers using Godot 4.4, providing powerful game engine features for your desktop backgrounds.

## Getting Started

### Creating a New Godot Wallpaper
The easiest way to get started is using our built-in wizard:

1. Open ScreenPlay
2. Go to the "Create" tab
3. Select "Godot Wallpaper"
4. Follow the wizard steps to set up your project

### Project Structure
Your Godot wallpaper project will include:
```plaintext
my_wallpaper/
├── project.json         # ScreenPlay project configuration
├── project.godot        # Godot project file
├── scenes/             # Your Godot scenes
├── scripts/            # GDScript scripts
└── assets/             # Resources (textures, audio, etc.)
```

## Development Notes

### Important Considerations
* ScreenPlay uses GDScript only (C# is not supported)
* Keep your project settings as generated
* Focus on simple, efficient designs for better performance
* Test your wallpaper at different resolutions

### Best Practices
* Keep it simple - complex scenes can impact system performance
* Minimize use of physics unless necessary
* Limit particle effects and dynamic lights
* Use efficient 2D rendering where possible
* Avoid unnecessary calculations in _process functions

## Export System

ScreenPlay handles the export process automatically:

### How It Works
1. When loading your wallpaper, ScreenPlay checks for a cached export package
2. If no package exists, it exports the project using the format:
   ```
   project-v{version}.zip
   ```
3. The export runs headlessly using the included Godot editor

### Cache Management
* Exports are versioned based on your project.json version
* To force a re-export:
  * Increment the version number in project.json
  * Or delete the existing project-v{version}.zip file

## Editing Your Wallpaper

### Quick Access
1. Right-click on your wallpaper in ScreenPlay
2. Select "Edit" from the context menu
3. The Godot Editor will open with your project

### Development Workflow
1. Make changes in the Godot Editor
2. Save your project
3. ScreenPlay will automatically re-export when needed

## Example Project

Here's a simple example of a Godot wallpaper:

```gdscript
extends Node2D

func _ready():
    # Keep initialization simple
    set_process(true)

func _process(delta):
    # Add minimal update logic
    # Avoid heavy calculations here
    pass
```

## Common Issues and Solutions

### Performance Issues
* Reduce scene complexity
* Minimize node count
* Use simple shaders
* Limit or avoid physics usage
* Reduce update frequency where possible

### Export Problems
* Check the Godot editor output for errors
* Verify all resources are properly included
* Ensure project.json version is correct

Need help? Visit our [community forum](https://forum.screen-play.app/) for support and to share your creations!