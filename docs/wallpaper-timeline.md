# ⏱️ Wallpaper Timeline


!!! tip  "🚀 Premium Feature"
     Wallpaper Timeline support is a premium feature. Your purchase helps tremendously in funding continued development - as a solo developer, every contribution directly supports new features and improvements. If you'd prefer, you can also compile ScreenPlay from source to unlock this feature for free - I believe in keeping this software open and accessible to everyone.

    ~ [Kelteseth](https://bsky.app/profile/kelteseth.bsky.social)


![timleine](timeline.png)

`C:\Users\USERNAME\AppData\Local\ScreenPlay\ScreenPlay\profiles.json`
``` json
{
    "profiles": [
        {
            "name": "default",
            "timelineWallpaper": [
                {
                    "endTime": "06:54:15",
                    "startTime": "00:00:00",
                    "wallpaper": [
                    ]
                },
                {
                    "endTime": "14:19:29",
                    "startTime": "06:54:15",
                    "wallpaper": [
                    ]
                },
                {
                    "endTime": "23:59:59",
                    "startTime": "14:19:29",
                    "wallpaper": [
                    ]
                }
            ],
            "widgets": [
            ]
        }
    ],
    "version": "2.0.0"
}

```