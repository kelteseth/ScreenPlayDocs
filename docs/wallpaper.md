# 🖼️ Wallpaper

### Video Wallpaper
Supported Formats (.mp3 .ogv .ogm .ogg .oga .opus .webm):

- [AV1](httpsV://en.wikipedia.org/wiki/AV1) Next generation open source codec!
- [VP8](https://en.wikipedia.org/wiki/VP8) & [VP9](https://en.wikipedia.org/wiki/VP9) also known as .webm

### HTML Wallpaper
Uses Chromium latest to display the content.

### QML Wallpaper
This display your qml file. Can have custom plugins and logic. You even can reuse the code from the Widgets, like displaying CPU infos!

### GIF Wallpaper
This display a gif. This is **very** performant.

### Video Wallpaper Performance

Because we use a free video codec (VP8/VP9) hardware acceleration can be difficult. 

#### If you are using intel integrated graphics it is highly recommended using the [latest windows 10 drivers](https://downloadcenter.intel.com/search?keyword=+10+DCH+Drivers+)

Intel supports VP8 for the longest time. [See wikipedia](https://en.wikipedia.org/wiki/Intel_Quick_Sync_Video#Hardware_decoding_and_encoding)
![Intel Video Support](intel.png "Fixed-function Quick Sync Video format support ")

AMD Support: [See Wikipedia](https://en.wikipedia.org/wiki/Unified_Video_Decoder#Format_support)
![AMD Video Support](amd.png "GPU support for recent AMD GPUs")

Nvidia Support: [See developer.nvidia.com (Scroll all the way down and click on the green GeForce/TITAN for older GPUs)](https://developer.nvidia.com/video-encode-decode-gpu-support-matrix)
![Nvidia Video Support](nvidia1.png "GPU support for recent Nvidia GPUs")
