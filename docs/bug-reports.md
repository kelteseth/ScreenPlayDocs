# 🐛 Reporting Bugs

This guide will help you effectively report bugs in ScreenPlay, ensuring they can be addressed quickly.

## Before Reporting

Before creating a new bug report, please check if the issue has already been reported:

1. Search the [Troubleshooting section](https://forum.screen-play.app/category/7/troubleshooting) on our forum
2. Check existing [bug reports](https://gitlab.com/kelteseth/ScreenPlay/issues?label_name%5B%5D=Bug) on GitLab

## Creating a Bug Report

### Step 1: Create an Account

If you don't already have one, [create a GitLab account](https://gitlab.com/users/sign_up).

### Step 2: Submit the Report

1. Go to our [New Issue](https://gitlab.com/kelteseth/ScreenPlay/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) page
2. Select the **Bug** template

![Selecting the Bug template in GitLab's new issue form](new_issue.png)

### Step 3: Fill Out the Report

Include the following essential information:

#### Required Information
* **Summary**: A clear, concise description of the bug
* **Steps to Reproduce**: Detailed steps that reliably trigger the issue
* **Current Behavior**: What actually happens when following the steps
* **Expected Behavior**: What should happen instead
* **Environment**: Your system details (OS, ScreenPlay version, etc.)

#### Additional Helpful Information
* **Example Project**: Attach relevant files (video, wallpaper, widget) as a ZIP
* **Screenshots/Recordings**: Visual evidence of the issue
* **Logs**: Any error messages or relevant system logs
* **Possible Fixes**: If you have insights into what might fix the issue

### Step 4: Add Labels

Select appropriate labels for your issue, such as:
* Operating system (Windows, Linux, MacOS)
* Component affected (UI, Wallpaper, Widget)
* Priority level
* Version number

The "Bug" label will be added automatically.

## Supporting Existing Issues

Help us prioritize bugs by voting on issues that affect you:

1. Find the issue in our [bug tracker](https://gitlab.com/kelteseth/ScreenPlay/issues?label_name%5B%5D=Bug)
2. Click the thumbs-up 👍 button to cast your vote

![Voting on an issue in GitLab](issue_vote.png)

Your votes help us understand which issues impact the most users and should be prioritized.

## Best Practices

* **Be Specific**: Include exact error messages and behaviors
* **One Issue Per Report**: Create separate reports for different bugs
* **Stay Active**: Monitor your report for questions from developers
* **Update**: Add new information if you discover more details
* **Be Patient**: Complex issues may take time to investigate and fix

Remember: The more detailed and well-organized your bug report is, the faster we can identify and fix the issue!