---
hide:
  - navigation
  - toc
---

# 🏠 Home

<div class="hero" markdown>

# ScreenPlay Documentation

Your knowledgebase for everything about ScreenPlay - from custom Wallpaper and Widgets to compling ScreenPlay yourself.

<div class="buttons" markdown>

[Building ScreenPlay](building-screenplay.md){ .md-button .md-button--primary }
[Read FAQ](frequently-asked-questions.md){ .md-button }
[Report Bug](bug-reports.md){ .md-button }
[Contribute](contribute.md){ .md-button }

</div>

</div>
