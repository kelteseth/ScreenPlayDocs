# 1. Download ScreenPlayDocs
``` bash
# HTTPS
git clone  https://gitlab.com/kelteseth/ScreenPlayDocs
```

### 2. Installation Python Windows
Install [python 3](https://www.python.org/downloads/)

### 2. Installation Python Linux
``` bash
# For Ubuntu 22.04 and newer we need to create an virutal env
sudo apt install python3.11 python3.11-venv -y
# Run this command in the ScreenPlay source root folder:
python3 -m venv env
# This must be run every time before running mkdocs serve!
source env/bin/activate
```

## 3. Install mkdocs dependencies:
``` bash
python -m pip install mkdocs mkdocs-material mkdocs-git-revision-date-localized-plugin
```

### Contributing
1. Run mkdocs locally
``` bash
mkdocs serve
```
2. Make changes
3. Create a pull request
